package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        // TODO: Implement Rock Paper Scissors
        //printing what round it is
        System.out.println("Let's play round " + roundCounter);
        while(true){
            //Getting input from user
            String userChoice = readInput("Your choice (Rock/Paper/Scissors)?").toLowerCase();

            //Seeing if user input is valid
            if(!userChoice.equals("rock") && !userChoice.equals("paper") && !userChoice.equals("scissors")) {
                System.out.println("I do not understand " + userChoice + ". " + "Could you try again?");
                continue;
            }
            //System choice in int
            int SystemChoice = (int) (Math.random()*3);
            //Translating system choice from int to string
            String SysChoice = "";
            if (SystemChoice == 0){
                SysChoice = "paper";
            }
            else if (SystemChoice == 1){
                SysChoice = "rock";
            }
            else if (SystemChoice == 2){
                SysChoice = "scissor";
            }

            //figure out who won
            String win = "";
            //tie
            if(userChoice.equals(SysChoice)){
                win = "It's a tie!";
            }
            
            //System win
            if (userChoice.equals("rock") && SysChoice.equals("paper") || userChoice.equals("scissor") && SysChoice.equals("rock") || userChoice.equals("paper") && SysChoice.equals("scissor")){
                win = "Computer wins!";
                computerScore+=1;
            }
            //User win
            else if (userChoice.equals("paper") && SysChoice.equals("rock") || userChoice.equals("rock") && SysChoice.equals("scissor") || userChoice.equals("scissor") && SysChoice.equals("paper")){
                win = "Human wins!";
                humanScore+=1;
            }
            //prints out who won
            System.out.println("Human chose " + userChoice + ", computer chose " + SysChoice + ". " + win);
            System.out.println("Score: human " + humanScore + ", computer " + computerScore);

            //adds to round counter
            roundCounter+=1;

            //Ask if user wants to continue
    
            String anw = readInput("Do you wish to continue playing? (y/n)?").toLowerCase();

            if (anw.equals("n")){
                System.out.println("Bye bye :)");
                break;
            }
            else{
                //printing what round it is
                System.out.print("Let's play round " + roundCounter + "\n");
            }

        }
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
